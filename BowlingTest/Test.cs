﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingTest
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp()]
        public void CreateGame()
        {
            game = new Game();
        }

        public void ManyRoll(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }

        public void AssertThat(int num)
        {
            Assert.That(game.Score(), Is.EqualTo(num));
        }

        [Test()]
        public void GutterGame()
        {
            ManyRoll(20, 0);

            AssertThat(0);
        }

        [Test()]
        public void onePinGame()
        {
            ManyRoll(20, 1);

            AssertThat(20);
        }

        [Test()]
        public void FirstFrameSpare()
        {
            game.Roll(5);
            game.Roll(5);
            ManyRoll(18, 1);

            AssertThat(29);
        }

        [Test()]
        public void FirstFrameStrike()
        {
            game.Roll(10);
            ManyRoll(18, 1);

            AssertThat(30);
        }

        [Test()]
        public void PerfectGame()
        {
            ManyRoll(12, 10);

            AssertThat(300);
        }

        [Test()]
        public void NormalGame()
        {
            game.Roll(10);
            game.Roll(9); game.Roll(1);
            game.Roll(5);game.Roll(5);
            game.Roll(7);game.Roll(2);
            game.Roll(10);
            game.Roll(10);
            game.Roll(10);
            game.Roll(9); game.Roll(0);
            game.Roll(8);game.Roll(2);
            game.Roll(9);game.Roll(1);game.Roll(10);

            AssertThat(187);
        }

        [Test()]
        public void LastFrameStrike()
        {
            ManyRoll(18, 1);
            game.Roll(10); game.Roll(1);game.Roll(1);

            AssertThat(30);
        }

        [Test()]
        public void LastFrameSpare()
        {
            ManyRoll(18, 1);
            game.Roll(5);game.Roll(5);game.Roll(1);

            AssertThat(29);
        }

        [Test()]
        public void AllSpareBonus10()
        {
            ManyRoll(20, 5);
            game.Roll(10);

            AssertThat(155);
        }
    }
}