﻿using System;

namespace BowlingGame
{
    public class Game
    {
        private int[] pinsFall = new int[21];
        private int rollCounter;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public void Roll(int pins)
        {
            pinsFall[rollCounter] += pins;
            rollCounter++;
        }

        public bool IsStrike(int i)
        {
            return pinsFall[i] == 10;
        }

        public bool IsSpare(int i)
        {
            return pinsFall[i] + pinsFall[i + 1] == 10;
        }

        public int StrikeBonus(int i)
        {
            return 10 + pinsFall[i + 1] + pinsFall[i + 2];
        }

        public int SpareBonus(int i)
        {
            return 10 + pinsFall[i + 2];
        }


        public int Score()
        {
            int score = 0;
            int i = 0;
            for (int frame = 0; frame < 10; frame++)
            {
                if (IsStrike(i))
                {
                    score += StrikeBonus(i);
                    i += 1;
                }
                else if (IsSpare(i))
                {
                    score += SpareBonus(i);
                    i += 2;
                }
                else
                {
                    score += pinsFall[i] + pinsFall[i + 1];
                    i += 2;  
                }
            }
            return score;
        }
    }
}
